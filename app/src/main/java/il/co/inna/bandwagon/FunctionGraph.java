package il.co.inna.bandwagon;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

import java.util.EventListener;
import java.util.Vector;

/**
 * Created by bratner on 3/1/15.
 */
public class FunctionGraph extends View {
    private static String TAG="BRAT_fgraph";
    private Vector<SineSpike> freqs;
    private Color lineColor;


    public FunctionGraph(Context context) {
        super(context);
        freqs = new Vector<SineSpike>();
        freqs.add(new SineSpike(4/(float)Math.PI,0, 1));
        freqs.add(new SineSpike(4/(3*(float)Math.PI),0, 3));
        freqs.add(new SineSpike(4/(5*(float)Math.PI),0, 5));
        freqs.add(new SineSpike(4/(7*(float)Math.PI),0, 7));
    }


    @Override
    protected void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        Paint p = new Paint();
        float[] points = new float[2*width];

        super.onDraw(canvas);

        if (freqs.size() == 0) {
            Log.d(TAG, "Nothing to show");
            return;
        }

        for(int i = 0; i< width; i++) {
            //points[i*2] = ((float)Math.PI)*((2*i/width)-1);
            points[i*2] = ((float)Math.PI)*((2*i/width)-1);
        }

        canvas.translate(width/2, height/2);
        canvas.scale(1,-1);
        p.setColor(Color.RED);
        p.setStrokeWidth(4);
        canvas.drawLine(-width/2, 0,width/2, 0,p);
        //Y-axis
        canvas.drawLine(0,-height/2, 0, height/2,p);

        canvas.scale(0.8f,0.8f);

        p.setStrokeWidth(2);


        for(SineSpike s: freqs ) {
            int pointX;
            int pointY;

            p.setColor(Color.BLUE);
            for(int scrX = 0; scrX < width; scrX++) {
                pointX = scrX * 2;
                pointY = pointX + 1;
                points[pointX] = scrX - (width/2.0f);
                points[pointY] += (height/2.0f)*s.getAmplitude()*(float)Math.sin(s.getOmega()*
                        (s.getPhase()+2*Math.PI*(points[pointX]))/width);
                //float valY = Math.sin((2*Math.PI*(scrX-width/2))/width);
                // our graph is mathematical thus exactly 2PI long
                // e.g scrX = - is -PI and sc
               // points[pointY] += height*valY;
                //canvas.drawPoint(points[pointX], points[pointY], p);

            }

            //canvas.drawLines(line10, 0,2,p);



            //canvas.drawLines(line10, 0, 2,p );
            //canvas.drawLine(-(float)Math.PI, 0.5F, (float)Math.PI, 0.5F,p);

        //    canvas.drawPoints(points, p);
        }
        p.setColor(Color.BLUE);
        canvas.drawLines(points,0, width*2,p);
        canvas.drawLines(points,2, width*2-2,p);


    }
}
