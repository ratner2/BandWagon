package il.co.inna.bandwagon;

/**
 * Created by bratner on 3/1/15.
 */
public class SineSpike {
    private double amplitude;
    private double phase;
    private double omega;

    public SineSpike() {
        this.amplitude = 0;
        this.phase = 0;
        this.omega = 0;
    }
    public SineSpike(double amplitude, double phase, double omega) {
        this.amplitude = amplitude;
        this.phase = phase;
        this.omega = omega;
    }

    public double getOmega() {
        return omega;
    }

    public void setOmega(double omega) {
        this.omega = omega;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    public double getPhase() {
        return phase;
    }

    public void setPhase(double phase) {
        this.phase = phase;
    }



}
